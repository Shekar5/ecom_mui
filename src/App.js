import logo from "./logo.svg";
import "./App.css";
import { createTheme, colors,ThemeProvider } from "@mui/material";

import Appbar from "./Components/Appbar";
import MenuBar from "./Components/menuBar";
import Carousel from "./Components/Carousel";
import Banner from "./Components/banner";
import Types from "./Components/trial";
import FlashDeals from "./Components/flashDeals";
import Categories from "./Components/Categories";
import MoreForYou from "./Components/moreForYou";
import BannerWithCarousel from "./Components/bannerWithCarousel";
import Footer from "./Components/Footer";

const theme=createTheme({
  // text:{
  //   main:'#2B3445',
  //   dark:'#0c0e30',
  //   light:'#373F50'
  // },
  palette:{
    primary:{
      main:'#2B3445',
      dark:'#0c0e30',
      light:'#373F50',
      fade:'#0F3460'
    },
    secondary:{
      main:'#D23F57',
      light:'#0C2A4D'
    },
    grey:{main:'#AEB4BE',
    light:'#7D879C'
// , dark:'#373f50'


    }
  }
})

function App() {
  return (<ThemeProvider theme={theme}>
    <div className="App">
      <Appbar />
      <MenuBar />
      <BannerWithCarousel />
      {/* < /> */}
      <FlashDeals />
      <Categories />
      <MoreForYou />
      <Footer />
    </div></ThemeProvider>
  );
}

export default App;

