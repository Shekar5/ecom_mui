import * as React from "react";

import { Button, Stack, Typography, Paper } from "@mui/material";

import nike from "../Images/nike-black.png";
function Banner() {
  const [selectedValue, setSelectedValue] = React.useState("a");

  const handleChange = (event) => {
    setSelectedValue(event.target.value);
  };

  return (
    <Paper>
      <Stack direction="row" spacing={1} m={10}>
        <Stack my={15} sx={{ alignItems: "center" }}>
          <Typography my={1} variant="h3">
            50% Off For Your First Shopping
          </Typography>
          <Typography my={3}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis
            lobortis consequat eu, quam etiam at quis ut convalliss.
          </Typography>
          <Button
            color="error"
            variant="contained"
            sx={{ width: "150px", height: "40px" }}
          >
            <Typography variant="body1" sx={{ color: "white" }}>
              Shop Now
            </Typography>
          </Button>
        </Stack>
        <img src={nike} />
      </Stack>
    </Paper>
    // <div>
    //   {selectedValue == "a" ? (
    //     <Stack direction="row" spacing={50} sx={{ mt: 5 }}>
    //       <img src={nike} width={500} />
    //       <Typography
    //         sx={{
    //           display: "flex",
    //           //   alignItems: "end",
    //             justifyContent: "center",
    //           alignContent: "center",
    //           alignSelf: "center",
    //         }}
    //       >
    //         Block1
    //       </Typography>
    //     </Stack>
    //   ) :(
    //     <Stack direction="row" spacing={50} sx={{ mt: 5 }}>
    //       <img src={nike} width={500} />
    //       <Typography
    //         sx={{
    //           display: "flex",
    //           //   alignItems: "end",
    //             justifyContent: "center",
    //           alignContent: "center",
    //           alignSelf: "center",
    //         }}
    //       >
    //         Block2
    //       </Typography>
    //     </Stack>
    //   ) }
    //   <Radio
    //     checked={selectedValue === "a"}
    //     onChange={(event) => handleChange(event)}
    //     value="a"
    //     name="radio-buttons"
    //     inputProps={{ "aria-label": "A" }}
    //   />
    //   <Radio
    //     checked={selectedValue === "b"}
    //     onChange={(event) => handleChange(event)}
    //     value="b"
    //     name="radio-buttons"
    //     inputProps={{ "aria-label": "B" }}
    //   />
    // </div>
  );
}

export default Banner;
