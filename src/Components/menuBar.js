import {
  AppBar,
  Toolbar,
  Button,
  Stack,
  Box,
  Input,
  TextField,
  InputBase,
  makeStyles,
  Typography,
} from "@mui/material";
import ButtonGroup from "@mui/material/ButtonGroup";
import CategoryOutlinedIcon from "@mui/icons-material/CategoryOutlined";
import ArrowForwardIosOutlinedIcon from "@mui/icons-material/ArrowForwardIosOutlined";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";

function MenuBar() {
  return (
    <Stack
      spacing={1}
      direction="row"
      sx={{ ml: 35, mt: 4, typography: "body1", width:'750%', color:'primary.main' }}
    >
      <Button
        variant="text"
        size="large"
        startIcon={<CategoryOutlinedIcon />}
        endIcon={
          <ArrowForwardIosOutlinedIcon fontSize="large" sx={{ ml: 15 }} />
        }
        sx={{ color:"black",textTransform:"none",fontWeight:"400" }}
      >
        Categories
      </Button>
      <Box sx={{ ml: 40 }}>
        <Button
          sx={{ ml: 20, color:"black",textTransform:"none", fontWeight:"400" }}
          variant="text"
          endIcon={<KeyboardArrowDownIcon />}
        >
          One
        </Button>
        <Button sx={{color:"black",textTransform:"none",fontWeight:"400" }} variant="text" endIcon={<KeyboardArrowDownIcon />}>
          Home
        </Button>
        <Button variant="text" sx={{color:"black",textTransform:"none",fontWeight:"400" }} endIcon={<KeyboardArrowDownIcon />}>
          Mega Menu
        </Button>
        <Button variant="text" sx={{color:"black",textTransform:"none",fontWeight:"400" }} endIcon={<KeyboardArrowDownIcon />}>
          Full Screen Menu
        </Button>
        <Button variant="text" sx={{color:"black",textTransform:"none",fontWeight:"400" }} endIcon={<KeyboardArrowDownIcon />}>
          Pages
        </Button>
        <Button variant="text" sx={{color:"black",textTransform:"none",fontWeight:"400" }} endIcon={<KeyboardArrowDownIcon />}>
          User Account
        </Button>
        <Button variant="text" sx={{color:"black",textTransform:"none",fontWeight:"400" }} endIcon={<KeyboardArrowDownIcon />}>
          Vendor Account
        </Button>
      </Box>
    </Stack>
  );
}

export default MenuBar;
