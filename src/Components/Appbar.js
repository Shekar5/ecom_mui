import {
  AppBar,
  Toolbar,
  Button,
  Stack,
  Box,
  Input,
  TextField,
  InputBase,
  makeStyles,
  Divider,
  IconButton,
} from "@mui/material";

import InputAdornment from "@mui/material/InputAdornment";
import SearchIcon from "@mui/icons-material/Search";
import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import ShoppingBagOutlinedIcon from "@mui/icons-material/ShoppingBagOutlined";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";

import './appBar.css'

import logo from "../Images/bazar-logo2.svg";
import { borderRadius } from "@mui/system";

function Appbar() {
  return (
    <Stack direction="row" sx={{ width: "70%", margin: "auto" }}>
      <Toolbar>
        <img src={logo} />
           
        <Box sx={{
            // borderRadius:'1000px'
            color:'primary.main'

        }}>
          {" "}
          <TextField className="textField"
            placeholder="searching for..."
            // borderRadius={1000}
            // startIcon={<SearchIcon />}
            size="small"
            sx={{ ml: 15, width: 700, height: 40, }}
            // inputProps={{startAdonrnment}

            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment position="end">
                  <Button
                  // onBeforeInput={<Divider/>}
                    sx={{
                      color: "primary.main",
                      textTransform: "none",
                      fontWeight: "400",
                      // border:'1px solid',
                      borderRadius:'50px'
                    }}
                    endIcon={<KeyboardArrowDownIcon />}
                  >
                    {/* <Divider /> */}
                    All Categories
                  </Button>
                </InputAdornment>
              )
            }}
          ></TextField>
          <IconButton sx={{ ml: 15 }}>
            <AccountCircleOutlinedIcon fontSize="large"></AccountCircleOutlinedIcon>
          </IconButton>
          <IconButton  sx={{ ml: 2 }}>
            <ShoppingBagOutlinedIcon fontSize="large" />
          </IconButton>
        </Box>
      </Toolbar>
    </Stack>
  );
}
export default Appbar;
