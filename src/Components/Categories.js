import * as React from "react";
import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import { Card, CardContent, CardMedia, Typography, Stack,Button } from "@mui/material";
import ArrowRightIcon from '@mui/icons-material/ArrowRight';

import categoryImage1 from "../Images/categoryImages/8 (1).webp";
// import categoryIcon from "./svg.js";
import CategoryOutlinedIcon from "@mui/icons-material/CategoryOutlined";

  function Categories() {
  const items = [
    {
      id: 1,
      category: "Automobile",
      image:
        "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FClothes%2F7.DenimClassicBlueJeans.png&w=64&q=75",
    },
    {
      id: 2,
      category: "Car",
      image:
        "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FClothes%2F20.GrayOvercoatWomen.png&w=64&q=75",
    },
    {
      id: 3,
      category: "Fashion",
      image:
        "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FJewellery%2F8.IndianPearlThreadEarrings.png&w=64&q=75",
    },
    {
      id: 4,
      category: "Mobile",
      image:
        "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FJewellery%2F8.IndianPearlThreadEarrings.png&w=64&q=75",
    },
    {
      id: 5,
      category: "Laptop",
      image:
        "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FShoes%2F11.Flowwhite.png&w=64&q=75",
    },
    {
      id: 6,
      category: "Desktop",
      image:
        "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FGroceries%2F1.SaktiSambarPowder.png&w=64&q=75",
    },
    {
      id: 7,
      category: "Tablet",
      image:
        "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FGroceries%2F14.ACIProducts.png&w=64&q=75",
    },
    {
      id: 8,
      category: "Fashion",
      image:
        "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FGroceries%2F27.SardinesPack.png&w=64&q=75",
    },
    {
      id: 9,
      category: "Electronics",
      image:
        "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FHealth%26Beauty%2F12.BeautySocietyantiacnemask.png&w=64&q=75",
    },
    {
      id: 10,
      category: "Furniture",
      image:
        "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FHealth%26Beauty%2F25.MarioBadescuSkinCareShampoo.png&w=64&q=75",
    },
    {
      id: 11,
      category: "Camera",
      image:
        "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FHome%26Garden%2F13.GardenRosesinBlueVase.png&w=64&q=75",
    },
    {
      id: 12,
      category: "Electronics",
      image:
        "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F12.Xiaomimiband2.png&w=64&q=75",
    },
  ];
  return (
    <Box my={2} sx={{width:'70%', margin:"auto",color:'primary.main'}}>
      <Stack direction="row" >
        <CategoryOutlinedIcon fontSize="medium" sx={{ color: "secondary.main",mr:'5px' }} />
        <Typography variant="h4" fontWeight={700} fontSize={'28px'}>Categories</Typography>
        <Button endIcon={<ArrowRightIcon/>} sx={{ textTransform: "none", color: "primary.light",ml:'950px' }}>
          View all
        </Button>
      </Stack>

      <Grid container spacing={3} my={1}>
        {items.map((item) => {
          return (
            <Grid
              item
              key={item.id}
              md={2}
              xs={2}
              sm={2}
              borderRadius="10px"
              
            >
              <Card>
                <Stack direction="row">
                  <CardMedia
                    component="img"
                    height="80px"
                    // width="40px"
                    image={item.image}
                  />
                  <CardContent>
                    <Typography variant="subtitle1">{item.category}</Typography>
                  </CardContent>
                </Stack>
              </Card>
            </Grid>
          );
        })}
      </Grid>
    </Box>
  );
}

export default Categories;

// const Item = styled(Paper)(({ theme }) => ({
//   backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
//   ...theme.typography.body2,
//   padding: theme.spacing(1),
//   textAlign: 'center',
//   color: theme.palette.text.secondary,
// }));

// function FormRow() {
//   return (
//     <React.Fragment>
//       <Grid item xs={4}>
//         <Item>Item</Item>
//       </Grid>
//       <Grid item xs={4}>
//         <Item>Item</Item>
//       </Grid>
//       <Grid item xs={4}>
//         <Item>Item</Item>
//       </Grid>
//     </React.Fragment>
//   );
// }

// export default function NestedGrid() {
//   return (
//     <Box sx={{ flexGrow: 1 }}>
//       <Grid container spacing={1}>
//         <Grid container item spacing={3}>
//           <FormRow />
//         </Grid>
//         <Grid container item spacing={3}>
//           <FormRow />
//         </Grid>
//         <Grid container item spacing={3}>
//           <FormRow />
//         </Grid>
//       </Grid>
//     </Box>
//   );
// }
