import Carousel from "react-elastic-carousel";
import Box from "@mui/material/Box";

import Banner from "./banner";
import { Visibility } from "@mui/icons-material";
import { Hidden } from "@mui/material";
// import './style2.css'

function BannerWithCarousel() {
    const items=[<Banner/>,<Banner/>]
    return ( 
        <Carousel itemsToShow={1} >
            {items.map(item=>{
                return(<Box>{item}</Box>)
            })}
        </Carousel>
     );
}

export default BannerWithCarousel;