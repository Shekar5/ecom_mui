import Carousel from "react-elastic-carousel";
import {
  AppBar,
  Toolbar,
  Button,
  Stack,
  Box,
  Input,
  TextField,
  InputBase,
  makeStyles,
  Typography,
  Card,
  CardHeader,
  CardContent,
  CardActions,
  CardMedia,
  IconButton,
  Rating,
} from "@mui/material";
import Grid from "@mui/material/Grid";
import ArrowRightIcon from '@mui/icons-material/ArrowRight';

import FavoriteIcon from "@mui/icons-material/Favorite";
import FavoriteBorderOutlinedIcon from "@mui/icons-material/FavoriteBorderOutlined";
import BoltIcon from "@mui/icons-material/Bolt";
// import Rating from '@mui/material/Rating';

import flash1 from "../Images/nike-black.png";
import flash4 from "../Images/flash-4.webp";
import flash2 from "../Images/flash-2.webp";
import flash3 from "../Images/flash-3.webp";
// import { imageListClasses } from '@mui/material';

import "./styles.css";

const items = [
  {
    id: 1,

    title: "Tarz T3",
    value: 4,
    price: "$164.70",
    strikePrice: 183.0,
    off: "10% off",
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FBikes%2F21.TarzT3.png&w=1920&q=75",
  },
  {
    id: 2,

    title: "Xamaha R15 Black",
    value: 2,
    price: "$162.00",
    strikePrice: 180.0,
    off: "10% off",
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FBikes%2F22.YamahaR15Black.png&w=1920&q=75",
  },
  {
    id: 3,

    title: "Xamaha R15 Blue",
    value: 2,
    price: "$249.30",
    strikePrice: 277.0,
    off: "10% off",
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FBikes%2F23.YamahaR15Blue.png&w=1920&q=75",
  },
  {
    id: 4,

    title: "Xevel 2020",
    value: 2,
    price: "$256.50",
    strikePrice: 270.0,
    off: "5% off",
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FBikes%2F24.Revel2020.png&w=1920&q=75",
  },
  {
    id: 5,

    title: "Jackson TB1",
    value: 3,
    price: "$112.10",
    strikePrice: 110.0,
    off: "5% off",
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FBikes%2F25.JacksonTB1.png&w=1920&q=75",
  },
  {
    id: 6,

    title: "Siri 2020",
    value: 4,
    price: "$122.20",
    strikePrice: 130.0,
    off: "6% off",
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F1.Siri2020.png&w=1920&q=75",
  },
  {
    id: 7,

    title: "COSOR1",
    value: 4,
    price: "$273.60",
    strikePrice: 288.0,
    off: "5% off",
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F2.COSOR1.png&w=1920&q=75",
  },
  {
    id: 8,

    title: "Ranasonic Charger",
    value: 5,
    price: "$107.10",
    strikePrice: 119.0,
    off: "10% off",
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F3.PanasonicCharge.png&w=1920&q=75",
  },
  {
    id: 9,

    title: "Lumix DSIR",
    value: 5,
    price: "$115.32",
    strikePrice: 124.0,
    off: "21% off",
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F3.PanasonicCharge.png&w=1920&q=75",
  },
  {
    id: 10,

    title: "Atech Cam 1080p",
    value: 3,
    price: "$264.60",
    strikePrice: 294.0,
    off: "10% off",
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F4.LumixDSLR.png&w=1920&q=75",
  },
  {
    id: 11,

    title: "Tony a9",
    value: 5,
    price: "$233.70",
    strikePrice: 246.0,
    off: "5% off",
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F5.AtechCam1080p.png&w=1920&q=75",
  },

  {
    id: 12,

    title: "beat sw3",
    value: 4,
    price: "$112.53",
    strikePrice: 121.0,
    off: "7% off",
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F6.Sonya9.png&w=1920&q=75",
  },
];

function MoreForYou() {
  return ( <Box ml={-3}><Stack direction="row" spacing={125} sx={{ ml: "0px",mb:'1px',mt:'50px' }}>
  <Typography variant="h4" ml={38} fontWeight={700} fontSize={'25px'} color={'primary.main'} >
    More For You
  </Typography>
  <Button endIcon={<ArrowRightIcon/>} sx={{ textTransform: "none", color: "primary.light",ml:'30px' }}>
    View all
  </Button>
</Stack>
    <Grid container spacing={3} sx={{width:'70%', margin:"auto"}}>
      {items.map((item) => {
        return (
         
          <Grid item md={3} key={item.id}>
            <Card
              key={item.id}
              borderRadius="10px"
              sx={{ height: "400px", width: "300px" }}
              // height={200}
              // width={30}
            >
              <Stack direction="row" spacing={21}>
                <CardContent>
                  <Typography
                    component="span"
                    bgcolor="secondary.main"
                    color="white"
                    sx={{
                      borderRadius: "10px",
                      fontSize: "12px",
                      padding: "5px",
                    }}
                  >
                    {item.off}
                  </Typography>
                </CardContent>
                <IconButton>
                  <FavoriteBorderOutlinedIcon />
                </IconButton>
              </Stack>
              <CardMedia
                component="img"
                height={200}
                //   fullSize
                image={item.image}
              />
              <CardContent>
                <Typography varient="body1" color={'primary.light'}fontWeight={400}>{item.title}</Typography>
                <Rating defaultValue={item.value} />
                <CardContent>
                  <Stack direction={"row"} spacing={9}>
                    <Stack direction="row" spacing={1}>
                      <Typography color="secondary.main">{item.price}</Typography>
                      <Typography color="grey.light" component="strike">
                        {item.strikePrice}
                      </Typography>
                    </Stack>
                    <Button
                      variant="outlined"
                    //   size='large'
                      sx={{
                        height: "28px",
                        // width: "1px",
                        border: "1px solid red",
                        color: "secondary.main",
                        fontSize: "25px",
                        
                      }}
                    >
                      +
                    </Button>
                  </Stack>
                </CardContent>
              </CardContent>
              <Stack></Stack>
            </Card>
          </Grid>
        );
      })}
    </Grid></Box>
  );
}

export default MoreForYou;
// function MoreForYou() {
//   return (
//     <Grid container spacing={3}>
//       {items.map((item) => ({
//        return (
//         <Grid item>HI</Grid>
//        )
//       }
// //          <Grid item xs={3} sm={3} md={3}>
// //           <Card
// //             key={item.id}
// //             borderRadius="10px"
// //             sx={{ height: "50px", width: "50px" }}
// //             height={200}
// //             // width={30}
// //           >
// //             <Stack direction="row" spacing={15}>
// //               <CardContent>
// //                 <Typography
// //                   component="span"
// //                   bgcolor="error.dark"
// //                   color="white"
// //                   sx={{
// //                     borderRadius: "10px",
// //                     fontSize: "12px",
// //                     padding: "5px",
// //                   }}
// //                 >
// //                   {item.off}
// //                 </Typography>
// //               </CardContent>
// //               <IconButton>
// //                 <FavoriteBorderOutlinedIcon />
// //               </IconButton>
// //             </Stack>
// //             <CardMedia
// //               component="img"
// //             //   height={200}
// //             //   fullSize
// //               image={item.name}
// //             />
// //             <CardContent>
// //               <Typography varient="h5">{item.title}</Typography>
// //               <Rating defaultValue={item.value} />
// //               <CardContent>
// //                 <Stack direction={"row"} spacing={5}>
// //                   <Stack direction="row" spacing={1}>
// //                     <Typography color="error.main">{item.price}</Typography>
// //                     <Typography component="strike">
// //                       {item.strikePrice}
// //                     </Typography>
// //                   </Stack>
// //                   <Button
// //                     variant="outlined"
// //                     sx={{
// //                       height: "25px",
// //                       width: "1px",
// //                       border: "1px solid red",
// //                       color: "error.main",
// //                       fontSize: "25px",
// //                     }}
// //                   >
// //                     +
// //                   </Button>
// //                 </Stack>
// //               </CardContent>
// //             </CardContent>
// //             <Stack></Stack>
// //           </Card>
// //         </Grid>
//       ))}
//     </Grid>
//   );
// }

// export default MoreForYou;
