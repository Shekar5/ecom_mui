import Carousel from "react-elastic-carousel";
import {
  AppBar,
  Toolbar,
  Button,
  Stack,
  Box,
  Input,
  TextField,
  InputBase,
  makeStyles,
  Typography,
  Card,
  CardHeader,
  CardContent,
  CardActions,
  CardMedia,
  IconButton,
  Rating,
} from "@mui/material";

import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import FavoriteIcon from "@mui/icons-material/Favorite";
import FavoriteBorderOutlinedIcon from "@mui/icons-material/FavoriteBorderOutlined";
import BoltIcon from "@mui/icons-material/Bolt";
// import Rating from '@mui/material/Rating';

import flash1 from "../Images/nike-black.png";
import flash4 from "../Images/flash-4.webp";
import flash2 from "../Images/flash-2.webp";
import flash3 from "../Images/flash-3.webp";
// import { imageListClasses } from '@mui/material';

// import "./styles.css";

const items = [
    {
      id: 1,
      name: flash1,
      title: "NikeCourt Zoom Vapor Cage",
      value: 4,
      price: "$187.50",
      strikePrice: 250.0,
      off: "25% off",
    },
    {
      id: 2,
      name: flash2,
      title: "Classic Rolex Watch",
      value: 3,
      price: "$297.00",
      strikePrice: 350.0,
      off: "15% off",
    },
    {
      id: 3,
      name: flash3,
      title: "IPhone 13 Pro Max",
      value: 5,
      price: "$108.00",
      strikePrice: 150.0,
      off: "28% off",
    },
    {
      id: 4,
      name: flash4,
      title: "Mi Led Smart Watch",
      value: 4,
      price: "$142.00",
      strikePrice: 180.0,
      off: "21% off",
    },
    {
      id: 5,
      name: flash1,
      title: "NikeCourt Zoom Vapor Cage",
      value: 4,
      price: "$187.50",
      strikePrice: 250.0,
      off: "25% off",
    },
    {
      id: 6,
      name: flash2,
      title: "Classic Rolex Watch",
      value: 3,
      price: "$297.00",
      strikePrice: 350.0,
      off: "15% off",
    },
    {
      id: 7,
      name: flash3,
      title: "IPhone 13 Pro Max",
      value: 5,
      price: "$108.00",
      strikePrice: 150.0,
      off: "28% off",
    },
    {
      id: 8,
      name: flash4,
      title: "Mi Led Smart Watch",
      value: 4,
      price: "$142.00",
      strikePrice: 180.0,
      off: "21% off",
    },
  ];
function FlashDeals() {
 
  return (
    <Box sx={{width:'70%', margin:"auto",color:'primary.main'}}>
      <Stack direction="row" spacing={111} sx={{ ml: "40px",mb:'25px' }}>
        <Typography variant="h4" fontWeight={700} fontSize={'30px'} >
          <BoltIcon fontSize="large" sx={{ color: "secondary.main" }} />
          FlashDeals
        </Typography>
        <Button endIcon={<ArrowRightIcon/>} sx={{ textTransform: "none", color: "primary.light" }}>
          View all
        </Button>
      </Stack>
      <Carousel itemsToShow={4}>
        {items.map((item) => (
          <Card key={item.id} sx={{width:"250px"}} borderRadius='10px'>
            <Stack direction="row" spacing={15}>
              <CardContent>
                <Typography
                  component="span"
                  bgcolor="secondary.main"
                  color="white"
                  sx={{
                    borderRadius: "10px",
                    fontSize: "12px",
                    padding: "5px",
                  }}
                >
                  {item.off}
                </Typography>
              </CardContent>
              <IconButton>
                <FavoriteBorderOutlinedIcon />
              </IconButton>
            </Stack>
            <CardMedia component="img" height={250} image={item.name} />
            <CardContent>
              <Typography varient="h5" color={'primary.light'}>{item.title}</Typography>
              <Rating defaultValue={item.value} />
              <CardContent>
                <Stack direction={"row"} spacing={5}>
                  <Stack direction="row" spacing={1}>
                    <Typography color="secondary.main">{item.price}</Typography>
                    <Typography color="grey.light" component="strike">
                      {item.strikePrice}
                    </Typography>
                  </Stack>
                  <Button
                    variant="outlined"
                    sx={{
                      height: "25px",
                      width: "1px",
                      border: "1px solid red",
                      color: "secondary.main",
                      fontSize: "25px",
                    }}
                  >
                    +
                  </Button>
                </Stack>
              </CardContent>
            </CardContent>
          </Card>
        ))}
      </Carousel>
    </Box>
  );
}

export default FlashDeals;
