import React, { useRef, useState } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
import {
    AppBar,
    Toolbar,
    Button,
    Stack,
    Box,
    Input,
    TextField,
    InputBase,
    makeStyles,
    Typography
  } from "@mui/material";
  import flash1 from "../Images/nike-black.png";
  import flash4 from "../Images/flash-4.webp";
  import flash2 from "../Images/flash-2.webp"
  import flash3 from "../Images/flash-3.webp"

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

// import "./styles.css";

// import required modules
import { Pagination, Navigation } from "swiper";


export default function Carousel() {
    const arr=[flash1,flash2,flash3,flash4,flash1,flash2,flash3,flash4];

  return (
    
      <Swiper
        slidesPerView={4}
        spaceBetween={10}
        slidesPerGroup={4}
        loop={true}
        navigation={true}
        modules={[Pagination, Navigation]}
        className="mySwiper"
      >
        {arr.map(slide=>{
            return (<SwiperSlide ><img src={slide} height={350} width={280}/></SwiperSlide>)
        })}
        
      </Swiper>
    
  );
}
