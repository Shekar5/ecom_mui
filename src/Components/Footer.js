import { ThumbUpOffAlt } from "@mui/icons-material";
import {
  Stack,
  Box,
  Typography,
  Button,
  Grid,
  IconButton,
  Icon,
} from "@mui/material";

import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import YouTubeIcon from '@mui/icons-material/YouTube';
import GoogleIcon from '@mui/icons-material/Google';
import TwitterIcon from '@mui/icons-material/Twitter';

import logo from "../Images/bazar-logo2.svg";
import playStoreIcon from "../Images/playstoreIcon.webp";
import appStoreLogo from "../Images/appStoreIMg.jpeg";


function Footer() {
  return (
    <Stack
      direction={"row"}
      p={11.5}
      spacing={15}
      mt={10}
      sx={{ backgroundColor: "primary.dark", justifyContent: "start" }}
    >
      <Box ml={20}>
        <Typography mb={2} sx={{display:'flex',justifyContent:'start'}}>
          <img src="https://bazar-react.vercel.app/assets/images/logo.svg" />
        </Typography>
        <Typography mb={1} variant="body2" color={"grey.main"}>
          Lorem ipsum sit amet, consectetur adipiscing elit.
        </Typography>
        <Typography mb={1} variant="body2" color={"grey.main"}>
          Auctor libero id et, in gravida. Sit diam mauris nulla
        </Typography>
        <Typography mb={5}  variant="body2" color={"grey.main"}>
          cursus. Erat et lectus vel ut sollicitudin elit at amet.
        </Typography>
        <Stack direction="row" spacing={4}>
          <Button sx={{ textTransform: "none", bgcolor: "primary.light" }}>
            <Stack direction="row" spacing={1}>
              <img width="30px" height={35} src={playStoreIcon} />
              <Box>
                <Typography sx={{ color: "white", fontSize: "10px" }}>
                  Get it on
                </Typography>
                <Typography
                  variant="h6"
                  sx={{ color: "white", fontSize: "15px" }}
                >
                  Google Play
                </Typography>
              </Box>
            </Stack>
          </Button>
          <Button sx={{ textTransform: "none", bgcolor: "primary.light",color: "white" }}>
            <Stack direction="row" spacing={1}>
              <img width="30px" height={35} src={appStoreLogo} />
              <Box textAlign="left">
                <Typography sx={{ color: "white", fontSize: "10px" }}>
                  Get it on
                </Typography>
                <Typography
                  variant="h6"
                  sx={{ color: "white", fontSize: "15px" }}
                >
                  App Store
                </Typography>
              </Box>
            </Stack>
          </Button>
        </Stack>
      </Box>
      <Box>
        <Stack>
          <Typography variant="h5" mb={1} fontWeight={400} color={"white"}>
            About Us
          </Typography>

          <Button 
            variant="text"
            sx={{ ml:'25px', color: "grey.main", textTransform: "none", fontWeight: "400",display:'flex', justifyContent:'start' }}
          >
            Careers
          </Button>
          <Button
            variant="text"
            sx={{ ml:'25px', color: "grey.main", textTransform: "none", fontWeight: "400",display:'flex', justifyContent:'start' }}
          >
            Our Stores
          </Button>
          <Button
            variant="text"
            sx={{  ml:'25px',color: "grey.main", textTransform: "none", fontWeight: "400",display:'flex', justifyContent:'start' }}
          >
            Our Cares
          </Button>
          <Button
            variant="text"
            sx={{  ml:'25px',color: "grey.main", textTransform: "none", fontWeight: "400",display:'flex', justifyContent:'start' }}
          >
            Terms & COnditions
          </Button>
          <Button
            variant="text"
            sx={{ ml:'25px', color: "grey.main", textTransform: "none", fontWeight: "400",display:'flex', justifyContent:'start' }}
          >
            Privacy Policy
          </Button>
        </Stack>
      </Box>
      <Box>
        <Stack>
          <Typography variant="h5" fontWeight={400} color={"white"}>
            Customer Care
          </Typography>

          <Button 
            variant="text"
            sx={{  ml:'25px',color: "grey.main", textTransform: "none", fontWeight: "400",display:'flex', justifyContent:'start' }}
          >
            Help Center
          </Button>
          <Button
            variant="text"
            sx={{  ml:'25px',color: "#AEB4BE", textTransform: "none", fontWeight: "400",display:'flex', justifyContent:'start' }}
          >
            How to Buy
          </Button>
          <Button
            variant="text"
            sx={{  ml:'25px',color: "grey.main",display:'flex', justifyContent:'start', textTransform: "none", fontWeight: "400" }}
          >
            Track Your Order
          </Button>
          <Button
            variant="text"
            sx={{ ml:'25px', color: "grey.main", display:'flex', justifyContent:'start',textTransform: "none", fontWeight: "400" }}
          >
            Corporate & Bulk Purchasing
          </Button>
          <Button
            variant="text"
            sx={{  ml:'25px',color: "grey.main", textTransform: "none", fontWeight: "400",display:'flex', justifyContent:'start' }}
          >
            Returns & Refunds
          </Button>
        </Stack>
      </Box>
      <Box >
        {/* <Stack> */}
        <Typography mb={1} variant="h5" color={"white"} fontWeight={400} sx={{display:'flex', justifyContent:'start'}}>
          Contact Us
        </Typography>
        <Typography variant="body2" sx={{display:'flex', justifyContent:'start'}} color={"grey.main"}>
          70 Washington Square South, New York, NY
        </Typography>
        <Typography sx={{display:'flex', justifyContent:'start'}} variant="body2" mb={1} color={"grey.main"}>
          10012, United States
        </Typography>
        <Typography variant="body2" mb={1} sx={{display:'flex', justifyContent:'start'}} color={"grey.main"}>
          Email: uilib.help@gmail.com
        </Typography>
        <Typography variant="body2" sx={{display:'flex', justifyContent:'start'}} color={"grey.main"}>
          Phone: +1 1123 456 780
        </Typography>

        <Stack direction='row' sx={{display:'flex', justifyContent:'start'}} spacing={1}>
            <IconButton sx={{color:"grey.main"}}><FacebookIcon /></IconButton>
            <IconButton sx={{color:"grey.main"}}> <YouTubeIcon/>  </IconButton>
            <IconButton sx={{color:"grey.main"}}> <GoogleIcon/> </IconButton>
            <IconButton sx={{color:"grey.main"}}> <InstagramIcon/></IconButton>
            <IconButton sx={{color:"grey.main"}} ><twitterIcon /> </IconButton>
        </Stack>
        {/* </Stack> */}
      </Box>
    </Stack>
  );
}

export default Footer;
